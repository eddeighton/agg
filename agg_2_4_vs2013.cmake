
##assuming THIRD_PARTY_DIR

######################################
#AGG
find_path( AGG_INCLUDE NAMES agg_math.h PATHS ${THIRD_PARTY_DIR}/agg/agg-2.4/include )
file( GLOB AGG_HEADERS ${AGG_INCLUDE}/*.h )
file( GLOB AGG_SRC ${AGG_INCLUDE}/../src/*.cpp )
source_group( agg FILES  ${AGG_HEADERS} ${AGG_SRC} )

include_directories( ${AGG_INCLUDE} )